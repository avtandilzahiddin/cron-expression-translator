# VERTEX Test Case
## <Assuming that you have Python3 installed on your system>

go to directory where vertext.py file is located and run:
```sh
python3 vertex.py "*/15 0 1,15 * 1-5"
```

## Note:
Please note that cron expression in terminal must be inside quote marks, for example:  

"* * * * *"