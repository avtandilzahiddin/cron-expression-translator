import sys

def read_cron(time_unit, max_value):
    if time_unit == '*':
        time_unit = [i for i in range(1, max_value + 1)]
    if '-' in time_unit:
        time_unit = time_unit.split('-')
        start = int(time_unit[0])
        end = int(time_unit[1]) + 1
        time_unit = [i for i in range(start, end)]
    if ',' in time_unit:
        time_unit = time_unit.split(',')
        time_unit = [int(i) for i in time_unit]
    if '/' in time_unit:
        time_unit = time_unit.split('/')
        time_unit = [i for i in range(0, max_value, int(time_unit[-1]))]
    return time_unit



cronstring = sys.argv[1].split()

minute = cronstring[0]
minute = read_cron(minute, 60)


hour = cronstring[1]
hour = read_cron(hour, 24)

day_of_month = cronstring[2]
day_of_month = read_cron(day_of_month, 30)

month = cronstring[3]
month = read_cron(month, 12)

day_of_week = cronstring[4]
day_of_week = read_cron(day_of_week, 7)

print('\n')
print("minute", *minute)
print("hour", *hour)
print('day_of_month', *day_of_month)
print('month', *month)
print('day_of_week', *day_of_week)
print('\n')

